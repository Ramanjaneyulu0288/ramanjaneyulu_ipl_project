// 1. Number of matches played per year for all the years in IPL.

const matchesPerYear = (matchesArr) => {
  let noMatches = {};
  matchesArr.map((eachMatch) => {
    if (noMatches[eachMatch["season"]]) {
      noMatches[eachMatch["season"]] += 1;
    } else {
      noMatches[eachMatch["season"]] = 1;
    }
  });

  return noMatches;
};

// 2. Number of matches won per team per year in IPL

function wonMatchesPerTeamPerYear(arr) {
  let wonMatchesObj = {};

  // Getting all the teams object

  arr.map((eachMatch, index, inputArr) => {
    if (wonMatchesObj[eachMatch["winner"]]) {
      let eachWinnerObject = wonMatchesObj[eachMatch["winner"]];

      if (eachWinnerObject[eachMatch["season"]]) {
        eachWinnerObject[eachMatch["season"]] += 1;
      } else {
        eachWinnerObject[eachMatch["season"]] = 1;
      }
    } else {
      wonMatchesObj[eachMatch["winner"]] = {};

      wonMatchesObj[eachMatch["winner"]][eachMatch["season"]] = 1;
    }
  });

  // Adding wins to the each team

  return wonMatchesObj;
}

// 3. Extra runs conceded per team in the year 2016

function extraRunsPerTeam(deliveriesArr, matchesArr, year) {
  let matchIds = {};
  matchesArr.forEach((eachMatch, index) => {
    if (eachMatch["season"] === `${year}`) {
      matchIds[eachMatch["id"]] = true
    }
  })





  let extrasRunsObject = {};

  deliveriesArr.map((eachBall, index, deliveriesArr) => {
    if (
      parseInt(eachBall["extra_runs"]) > 0 &&
      eachBall["match_id"] in matchIds
    ) {
      if (extrasRunsObject[eachBall["bowling_team"]] >= 0) {
        extrasRunsObject[eachBall["bowling_team"]] += parseInt(
          eachBall["extra_runs"]
        );
      } else {
        extrasRunsObject[eachBall["bowling_team"]] = 0;
      }
    }
  });

  return extrasRunsObject;
}

// 4. Top 10 economical bowlers in the year 2015

function findEconomicalBowlers(deliveriesArr, matchesArr,year) {
  let matchIds = {};
  matchesArr.forEach((eachMatch, index) => {
    if (eachMatch["season"] === `${year}`) {
      matchIds[eachMatch["id"]] = true
    }
  })



  let bowlers = {};

  deliveriesArr.map((eachDelivery, index) => {
    if (eachDelivery["match_id"] in matchIds) {
     
      if (bowlers[eachDelivery["bowler"]]) {
        bowlers[eachDelivery["bowler"]]["total_runs"] += parseInt(
          eachDelivery["total_runs"]
        );

        bowlers[eachDelivery["bowler"]]["total_balls"] += 1;
      } else {
        bowlers[eachDelivery["bowler"]] = {
          total_runs: parseInt(eachDelivery["total_runs"]),
          total_balls: 1,
        };
      }
    }
  });
  //console.log(bowlers)
  let averageOfBowlers = [];

  Object.keys(bowlers).map((eachBowler) => {
    let overs = bowlers[eachBowler]["total_balls"] / 6;
    let avg = bowlers[eachBowler]["total_runs"] / overs;

    averageOfBowlers.push([eachBowler, avg]);
  });

  averageOfBowlers.sort(function (a, b) {
    return a[1] - b[1];
  });

  return averageOfBowlers.slice(0, 10);
}

// 5.1 Number of times each team won the toss and also won the match



const wonTossAndMatch = (matchesArr) => {

  let resultObj = {}

matchesArr.filter((eachMatch,index,matchesArr) => {
  if(eachMatch["winner"] === eachMatch["toss_winner"]){

if(resultObj[eachMatch["winner"]]){
  resultObj[eachMatch["winner"]]["total_won"] += 1
}else{
  resultObj[eachMatch["winner"]] = {}
  resultObj[eachMatch["winner"]]["total_won"] = 1
}



  }
})
return resultObj
}


// 5.2 Highest no.of Player of the Match for each season

const playerOfTheMatch = (matchesArr) => {
  let totalPlayersOfTheMatch = {}

matchesArr.map((eachMatch,index,matchesArr) => {


  if(totalPlayersOfTheMatch[eachMatch["player_of_match"]]){
    totalPlayersOfTheMatch[eachMatch["player_of_match"]] += 1
  }else{
    totalPlayersOfTheMatch[eachMatch["player_of_match"]] = 1
  }
})

let arrayOfPlayerOfTheMatch = Object.entries(totalPlayersOfTheMatch)

arrayOfPlayerOfTheMatch.sort((a,b) => {
  return b[1] - a[1]
})

return arrayOfPlayerOfTheMatch[0]

}



module.exports = {
  matchesPerYear,
  wonMatchesPerTeamPerYear,
  extraRunsPerTeam,
  findEconomicalBowlers,
  wonTossAndMatch,
  playerOfTheMatch
};
