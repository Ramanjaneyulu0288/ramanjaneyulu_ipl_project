const fs = require("fs");
const path = require("path");
const csv = require("csvtojson");

const myFunctions = require("./ipl");

const displayResults = async () => {
 
 
  const matchesArray = await csv().fromFile("../data/matches.csv");
  const deliveriesArr = await csv().fromFile("../data/deliveries.csv"); 

//console.log(matchesArray)

  let matchesPerYearObj = myFunctions.matchesPerYear(matchesArray);
  let wonMatchesPerTeamPerYearObj =
    myFunctions.wonMatchesPerTeamPerYear(matchesArray);
  let extraRuns = myFunctions.extraRunsPerTeam(deliveriesArr,matchesArray,2016);

  let economicBowlers = myFunctions.findEconomicalBowlers(
    deliveriesArr,
    matchesArray,2015
  );

  let noOfWonTossAndMatch = myFunctions.wonTossAndMatch(matchesArray)

  let highestPlayerOfTheMatch = myFunctions.playerOfTheMatch(matchesArray)

console.log(highestPlayerOfTheMatch)

  // Dumping into JSON files

  fs.writeFileSync(
    path.resolve(__dirname, "../public/output/matchesPerYear.json"),
    JSON.stringify(matchesPerYearObj)
  );

  fs.writeFileSync(
    path.resolve(__dirname, "../public/output/wonMatchesPerTeamPerYear.json"),
    JSON.stringify(wonMatchesPerTeamPerYearObj)
  );

  fs.writeFileSync(
    path.resolve(__dirname, "../public/output/extraRunsPerTeam.json"),
    JSON.stringify(extraRuns)
  );

  fs.writeFileSync(
    path.resolve(__dirname, "../public/output/economicBowlers.json"),
    JSON.stringify(economicBowlers)
  );
};

displayResults();








